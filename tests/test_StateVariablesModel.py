# SPDX-License-Identifier: MIT
# Copyright (c) 2018 University of Zurich

import os
import sys
import unittest
from contextlib import contextmanager

import numpy as np
from brian2 import us, ms, prefs, defaultclock, start_scope, SpikeGeneratorGroup, SpikeMonitor, StateMonitor

from SNN_data_visualizer.DataModels import StateVariablesModel
from utils_unittests import run_brian_network


@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout

class TestDataModel(unittest.TestCase):

    def test_StateVariablesModel(self):

        state_variable_names = ['var_name']
        num_neurons = 6
        num_timesteps = 50
        state_variables = [np.random.random((num_neurons, num_timesteps))]
        state_variables_times = [np.linspace(0, 100, num_timesteps)]

        SVM = StateVariablesModel(
            state_variable_names,
            state_variables,
            state_variables_times)

        self.assertTrue(SVM.var_name.shape == (num_neurons, num_timesteps))
        self.assertTrue(len(SVM.t_var_name) == num_timesteps)


        # test that raise Exception if variable names are not unique
        state_variable_names = ['var_name', 'var_name']
        num_neurons = 6
        num_timesteps = 50
        state_variables = [np.random.random((num_neurons, num_timesteps)), np.random.random((num_neurons, num_timesteps))]
        state_variables_times = [np.linspace(0, 100, num_timesteps), np.linspace(0, 100, num_timesteps)]

        with suppress_stdout():
            with self.assertRaises(Exception) as context:
                SVM = StateVariablesModel(
                    state_variable_names,
                    state_variables,
                    state_variables_times)


    def test_StateVariablesModelfrombrianstatemonitors(self):

        spikemonN1, spikemonN2, statemonN1, statemonN2 = run_brian_network()

        SVM = StateVariablesModel.from_brian_state_monitors(
            [statemonN1, statemonN2], skip_not_rec_neuron_ids=False)
        self.assertTrue(SVM.v1.shape[0] == len(statemonN1.t))
        self.assertTrue(len(SVM.t_v1) == len(statemonN1.t))
        self.assertTrue(SVM.v2.shape[0] == len(statemonN2.t))
        self.assertTrue(len(SVM.t_v2) == len(statemonN2.t))

        SVM = StateVariablesModel.from_brian_state_monitors(
            [statemonN1, statemonN2], skip_not_rec_neuron_ids=True)
        self.assertTrue(SVM.v1.shape[1] == len(statemonN1.record))
        self.assertTrue(SVM.v2.shape[1] == len(statemonN2.record))

        # statemonN1 & statemonN2_2 store the a variable called 'v1'
        with suppress_stdout():
            with self.assertRaises(Exception) as context:
                SVM = StateVariablesModel.from_brian_state_monitors(
                    [statemonN1, statemonN2_2], skip_not_rec_neuron_ids=False)


if __name__ == '__main__':
    unittest.main()
