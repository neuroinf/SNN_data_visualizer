# SPDX-License-Identifier: MIT
# Copyright (c) 2018 University of Zurich

import numpy as np

from brian2 import us, ms, second, prefs, defaultclock, start_scope, SpikeGeneratorGroup, SpikeMonitor, StateMonitor, NeuronGroup, Synapses, run

from SNN_data_visualizer.DataViewers import PlotSettings


def get_plotsettings(alpha=None, max255=False):

    if alpha is not None:
        colors = np.asarray([(1,0,0, alpha),  # 'r',
                             (0,1,0, alpha),  # 'g',
                             (0,0,1, alpha)])  # 'b'])
        if max255:
            colors = colors * 255.

    else:
        colors = ['r', 'g', 'b']

    MyPlotSettings = PlotSettings(
        fontsize_title=20,
        fontsize_legend=14,
        fontsize_axis_labels=14,
        marker_size=30,
        colors=colors)
    return MyPlotSettings

def run_brian_network(spikemonitors=True, statemonitors=True):
    prefs.codegen.target = "numpy"
    defaultclock.dt = 10 * us

    start_scope()
    N_input, N_N1, N_N2 = 1, 5, 3
    duration_sim = 30  # ms
    tau = 10*ms

    eqs1 = '''
    dv1/dt = -v1/tau : 1
    '''
    eqs2 = '''
    dv2/dt = -v2/tau : 1
    '''

    # setup spike generator
    spikegen_spike_times = np.sort(np.random.choice(size=500, a=np.arange(float(defaultclock.dt), float(duration_sim*ms)*0.9,
                                                                          float(defaultclock.dt*5)), replace=False)) * second
    spikegen_neuron_ids = np.zeros_like(spikegen_spike_times) / ms
    gInpGroup = SpikeGeneratorGroup(N_input, spikegen_neuron_ids, spikegen_spike_times)

    # setup neurons
    testNeurons1 = NeuronGroup(N_N1, eqs1, threshold='v1>1', reset='v1=0', method='exact')
    testNeurons2 = NeuronGroup(N_N2, eqs2, threshold='v2>1', reset='v2=0', method='exact')

    # setup connections
    InpSyn = Synapses(gInpGroup, testNeurons1, on_pre='v1 += 200 + rand() * 100', method='exact')
    InpSyn.connect()
    Syn = Synapses(testNeurons1, testNeurons2, on_pre='v2 += 200 + rand() * 100', method='exact')
    Syn.connect()


    returns = []
    if spikemonitors:
        # spike monitors input and network
        spikemonN1 = SpikeMonitor(testNeurons1, name='spikemon')
        spikemonN2 = SpikeMonitor(testNeurons2, name='spikemonOut')
        returns.extend([spikemonN1, spikemonN2])

    if statemonitors:
        # state monitor neurons
        statemonN1 = StateMonitor(testNeurons1, variables=["v1"], record=True, name='statemonNeu')
        statemonN2 = StateMonitor(testNeurons2, variables=["v2"], record=True, name='statemonNeuOut')
        returns.extend([statemonN1, statemonN2])

    run(duration_sim * ms)
    return returns