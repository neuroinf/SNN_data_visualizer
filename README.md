## Requirements for `SNN_data_visualizer`
* matplotlib>=2.2.2
* Brian2>=2.1.3.1
* numpy>=1.14.5
* pyqtgraph>=0.10.0
* pyqt5>=5.10.1

## Getting Started with `SNN_data_visualizer`

*  clone the repository
    ``` bash
    git clone https://gitlab.com/neuroinf/SNN_data_visualizer.git
    ```

* Find more detailed explanations in the folder 'examples_scripts'

## License
_`SNN_data_visualizer`_ is licenced under the MIT license, see the `LICENSE` file.


## History

The `SNN_data_visualizer` was built as a part of teili:

teili, das /taɪli/, Swiss german diminutive for piece. <br />

This toolbox was developed to provide computational neuroscientists and neuromorphic engineers with a playground for implementing neural algorithms which are simulated using **brian2**.<br />
You can find the documentation to teili [here](https://teili.readthedocs.io/en/latest/).

