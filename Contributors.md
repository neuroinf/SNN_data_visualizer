# Contributors

*  **Renate Krause** - Code visualizer framework Model/View/Controllers
*  **Adrian M. Whatley** - Code management, review, integration
